var dir_0462a99955ba9df75cf124212c64e434 =
[
    [ "CustomizeGraphy.cs", "_customize_graphy_8cs.html", [
      [ "CustomizeGraphy", "class_tayx_1_1_graphy_1_1_customization_scene_1_1_customize_graphy.html", null ]
    ] ],
    [ "ForceSliderToMultipleOf3.cs", "_force_slider_to_multiple_of3_8cs.html", [
      [ "ForceSliderToMultipleOf3", "class_tayx_1_1_graphy_1_1_customization_scene_1_1_force_slider_to_multiple_of3.html", null ]
    ] ],
    [ "ForceSliderToPowerOf2.cs", "_force_slider_to_power_of2_8cs.html", [
      [ "ForceSliderToPowerOf2", "class_tayx_1_1_graphy_1_1_customization_scene_1_1_force_slider_to_power_of2.html", null ]
    ] ],
    [ "UpdateTextWithSliderValue.cs", "_update_text_with_slider_value_8cs.html", [
      [ "UpdateTextWithSliderValue", "class_tayx_1_1_graphy_1_1_customization_scene_1_1_update_text_with_slider_value.html", null ]
    ] ]
];