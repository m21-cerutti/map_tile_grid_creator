var dir_bbfa9dfdea64a6cb3255a55088b76701 =
[
    [ "Advanced", "dir_45f92afe9bb77911f5960a56f821d068.html", "dir_45f92afe9bb77911f5960a56f821d068" ],
    [ "Audio", "dir_a3acbfcd9416372269c3d2ffea5cdbe5.html", "dir_a3acbfcd9416372269c3d2ffea5cdbe5" ],
    [ "Editor", "dir_36eaeb64c55d7e04096066ea82c91385.html", "dir_36eaeb64c55d7e04096066ea82c91385" ],
    [ "Fps", "dir_1c86c983011c6dcf850a7a227f5fd10f.html", "dir_1c86c983011c6dcf850a7a227f5fd10f" ],
    [ "Graph", "dir_d25d4b2d46b98d4a925c84023f76718b.html", "dir_d25d4b2d46b98d4a925c84023f76718b" ],
    [ "Ram", "dir_4c1127ae7a484380b40af53abc064cb9.html", "dir_4c1127ae7a484380b40af53abc064cb9" ],
    [ "Shader", "dir_96221b152f7b3a2004b19b216696f026.html", "dir_96221b152f7b3a2004b19b216696f026" ],
    [ "UI", "dir_58bed55fd156183f65f3be192298f758.html", "dir_58bed55fd156183f65f3be192298f758" ],
    [ "Util", "dir_cadf61ddd108ba19ba8203f4f5ac1b3a.html", "dir_cadf61ddd108ba19ba8203f4f5ac1b3a" ],
    [ "GraphyDebugger.cs", "_graphy_debugger_8cs.html", [
      [ "GraphyDebugger", "class_tayx_1_1_graphy_1_1_graphy_debugger.html", "class_tayx_1_1_graphy_1_1_graphy_debugger" ],
      [ "DebugCondition", "struct_tayx_1_1_graphy_1_1_graphy_debugger_1_1_debug_condition.html", "struct_tayx_1_1_graphy_1_1_graphy_debugger_1_1_debug_condition" ],
      [ "DebugPacket", "class_tayx_1_1_graphy_1_1_graphy_debugger_1_1_debug_packet.html", "class_tayx_1_1_graphy_1_1_graphy_debugger_1_1_debug_packet" ]
    ] ],
    [ "GraphyManager.cs", "_graphy_manager_8cs.html", [
      [ "GraphyManager", "class_tayx_1_1_graphy_1_1_graphy_manager.html", "class_tayx_1_1_graphy_1_1_graphy_manager" ]
    ] ]
];