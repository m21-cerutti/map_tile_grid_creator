var class_tayx_1_1_graphy_1_1_audio_1_1_g___audio_monitor =
[
    [ "dBNormalized", "class_tayx_1_1_graphy_1_1_audio_1_1_g___audio_monitor.html#af4d65f23129bf2ba080baba2a63c7f3e", null ],
    [ "lin2dB", "class_tayx_1_1_graphy_1_1_audio_1_1_g___audio_monitor.html#af2e6e9d30917c150005798b20bf6fd9e", null ],
    [ "UpdateParameters", "class_tayx_1_1_graphy_1_1_audio_1_1_g___audio_monitor.html#ab99d3ec55c039eb4c3cdfe46a094bccd", null ],
    [ "MaxDB", "class_tayx_1_1_graphy_1_1_audio_1_1_g___audio_monitor.html#a885c3428be1446004b38185ed31568dc", null ],
    [ "Spectrum", "class_tayx_1_1_graphy_1_1_audio_1_1_g___audio_monitor.html#ae5703e4a87b16703a147f0d39970b507", null ],
    [ "SpectrumDataAvailable", "class_tayx_1_1_graphy_1_1_audio_1_1_g___audio_monitor.html#a84e1cabce3fd229a904fb62ebeb508dc", null ],
    [ "SpectrumHighestValues", "class_tayx_1_1_graphy_1_1_audio_1_1_g___audio_monitor.html#a9eb17c5cdc4bf8c7f81222bb8a3eb9e6", null ]
];